import pprint

def f1(text):
    print("received: ", text)

def f2(age):
    print("age is ", age)

def f3(name):
    print("name is ", name)
    f1("name printed")

def printinfo(age, name):
    f2(age)
    f3(name)

def inc(a):
    return a+1

def sum(a,b):
    pprint.pprint(a)
    pprint.pprint(b)
    return a+b

def PPrintsumandone(a,b):
    newa = inc(a)
    newb = inc(b)
    c = sum(newa, newb)
    return c

if __name__ == "__main__":
    PPrintsumandone(4, 5)
    f1("start test")
    age = 33
    name = "ali"
    printinfo(age, name)